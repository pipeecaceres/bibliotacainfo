package com.pipe.biblioteca.modelos;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-07-03T11:50:46")
@StaticMetamodel(Distribuidor.class)
public class Distribuidor_ { 

    public static volatile SingularAttribute<Distribuidor, Integer> id_dis;
    public static volatile SingularAttribute<Distribuidor, String> rut;
    public static volatile SingularAttribute<Distribuidor, Date> año_contrato;
    public static volatile SingularAttribute<Distribuidor, Integer> numero;
    public static volatile SingularAttribute<Distribuidor, String> calle;
    public static volatile SingularAttribute<Distribuidor, String> comuna;
    public static volatile SingularAttribute<Distribuidor, String> nombre_dis;
    public static volatile SingularAttribute<Distribuidor, Integer> fono;
    public static volatile SingularAttribute<Distribuidor, String> pais;

}