package com.pipe.biblioteca.modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-07-03T11:50:47")
@StaticMetamodel(Autor.class)
public class Autor_ { 

    public static volatile SingularAttribute<Autor, String> ape_paterno;
    public static volatile SingularAttribute<Autor, String> nombre_au;
    public static volatile SingularAttribute<Autor, String> ape_materno;
    public static volatile SingularAttribute<Autor, Integer> cod_autor;

}