package com.pipe.biblioteca.modelos;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-07-03T11:50:46")
@StaticMetamodel(Factura.class)
public class Factura_ { 

    public static volatile SingularAttribute<Factura, Integer> meotodo_pago_cod_pago;
    public static volatile SingularAttribute<Factura, Integer> distribuidor_id;
    public static volatile SingularAttribute<Factura, Integer> precio_neto;
    public static volatile SingularAttribute<Factura, Date> hora;
    public static volatile SingularAttribute<Factura, Date> fecha_compra;
    public static volatile SingularAttribute<Factura, Integer> folio_fac;

}