package com.pipe.biblioteca.modelos;

import com.pipe.biblioteca.modelos.Autor;
import com.pipe.biblioteca.modelos.Categoria;
import com.pipe.biblioteca.modelos.Idioma;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-07-03T11:50:47")
@StaticMetamodel(Libro.class)
public class Libro_ { 

    public static volatile SingularAttribute<Libro, Integer> paginas;
    public static volatile SingularAttribute<Libro, Integer> editorial_cod_edit;
    public static volatile SingularAttribute<Libro, Integer> precio;
    public static volatile SingularAttribute<Libro, Integer> estado_cod_estado;
    public static volatile SingularAttribute<Libro, String> isbn;
    public static volatile ListAttribute<Libro, Categoria> categoria;
    public static volatile SingularAttribute<Libro, Integer> serie;
    public static volatile SingularAttribute<Libro, String> titulo;
    public static volatile ListAttribute<Libro, Idioma> idioma;
    public static volatile SingularAttribute<Libro, Date> año;
    public static volatile ListAttribute<Libro, Autor> autor;

}