package com.pipe.biblioteca.modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-07-03T11:50:47")
@StaticMetamodel(Compra.class)
public class Compra_ { 

    public static volatile SingularAttribute<Compra, String> compracol;
    public static volatile SingularAttribute<Compra, Integer> precio;
    public static volatile SingularAttribute<Compra, Integer> libro_serie;
    public static volatile SingularAttribute<Compra, Integer> factura_folio_fac;
    public static volatile SingularAttribute<Compra, Integer> id_comp;

}