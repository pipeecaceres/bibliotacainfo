/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;


import com.pipe.biblioteca.modelos.Autor;
import com.pipe.biblioteca.modelos.AutorManager;
import com.pipe.biblioteca.modelos.Categoria;
import com.pipe.biblioteca.modelos.CategoriaManager;
import com.pipe.biblioteca.modelos.Compra;
import com.pipe.biblioteca.modelos.CompraManager;
import com.pipe.biblioteca.modelos.Distribuidor;
import com.pipe.biblioteca.modelos.DistribuidorManager;
import com.pipe.biblioteca.modelos.Editorial;
import com.pipe.biblioteca.modelos.EditorialManger;

import com.pipe.biblioteca.modelos.Estado;
import com.pipe.biblioteca.modelos.EstadoManager;
import com.pipe.biblioteca.modelos.Factura;
import com.pipe.biblioteca.modelos.FacturaManager;
import com.pipe.biblioteca.modelos.Idioma;
import com.pipe.biblioteca.modelos.IdiomaManager;

import com.pipe.biblioteca.modelos.Libro;
import com.pipe.biblioteca.modelos.LibroManager;
import com.pipe.biblioteca.modelos.Pago;
import com.pipe.biblioteca.modelos.PagoManager;
import com.pipe.biblioteca.vistas.Menu;
import java.util.Scanner;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 clase main del sw 
 */
public class Biblioteca {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Menu Inicio = new Menu();
        Inicio.setVisible(true);
        
        
        
        System.out.println("Acontinuacion se haran pruebas de persistencia en");
        System.out.println("la base de datos:");
        System.out.println("----------------------------------------------");
        System.out.println("");
        System.out.println("Primero ingresamos los datos de un Idioma:");
        System.out.print("Ingrese lengua: ");

        Scanner scanner = new Scanner(System.in);
        String lengua = scanner.nextLine();

        IdiomaManager idiomaManager = new IdiomaManager();
        Idioma idioma = idiomaManager.crearIdioma(lengua);

        System.out.println("----------------------------------------------");
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(idioma.toString());

        System.out.print("Ingrese Categoria: ");
        String nombre = scanner.nextLine();

        CategoriaManager categoriaManager = new CategoriaManager();
        Categoria categoria = categoriaManager.crearCategoria(nombre);
        System.out.println("----------------------------------------------");
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(categoria.toString());

        System.out.print("Ingrese Estado: ");
        String descripcion = scanner.nextLine();

        EstadoManager estadoManager = new EstadoManager();
        Estado estado = estadoManager.crearEstado(descripcion);
        System.out.println("----------------------------------------------");
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(estado.toString());

        System.out.print("Ingrese Editorial: ");
        String nombre_edi = scanner.nextLine();

        EditorialManger editorialManager = new EditorialManger();
        Editorial editorial = editorialManager.crearEditorial(nombre_edi);
        System.out.println("----------------------------------------------");
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(editorial.toString());

        System.out.print("Ingrese Nombre Autor: ");
        String nombre_au = scanner.nextLine();
        System.out.print("Ingrese Apellido Paterno del Autor: ");
        String ape_parerno = scanner.nextLine();
        System.out.print("Ingrese Apellido Materno del Autor: ");
        String ape_materno = scanner.nextLine();

        AutorManager autorManager = new AutorManager();
        Autor autor = autorManager.crearAutor(nombre_au, ape_parerno, ape_materno);
        System.out.println("----------------------------------------------");
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(autor.toString());

        System.out.print("Ingrese Nombre Distribuidor: ");
        String nombre_dis = scanner.nextLine();
        System.out.print("Ingrese calle de distribuidor: ");
        String calle = scanner.nextLine();
        System.out.print("Ingrese nuemero de calle: ");
        String numeroo = scanner.nextLine();
        int numero = Integer.parseInt(numeroo);
        System.out.print("Ingrese comuna: ");
        String comuna = scanner.nextLine();
        System.out.print("Ingrese pais: ");
        String pais = scanner.nextLine();
        System.out.print("Ingrese fono del distribuidor: ");
        String fonoo = scanner.nextLine();
        int fono = Integer.parseInt(fonoo);
        System.out.print("Ingrese rut del distribuidor: ");
        String rut = scanner.nextLine();

        DistribuidorManager distribuidorManger = new DistribuidorManager();
        Distribuidor distribuidor = distribuidorManger.crearDistribuidor(nombre_dis, calle, numero, comuna, pais, fono, rut);
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(distribuidor.toString());

        System.out.print("Ingrese descripion del pago: ");
        String descripcion_pag = scanner.nextLine();

        PagoManager pagoManager = new PagoManager();
        Pago pago = pagoManager.crearPago(descripcion_pag);
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(pago.toString());
        
        System.out.print("Ingrese isbn: ");
        String isbn = scanner.nextLine();

        System.out.print("Ingrese titulo: ");
        String titulo = scanner.nextLine();

        System.out.print("Ingrese paginas: ");
        String paginass = scanner.nextLine();
        int paginas = Integer.parseInt(paginass);


        System.out.print("Ingrese precio: ");
        String precioo = scanner.nextLine();
        int precio = Integer.parseInt(precioo);

        LibroManager libroManager = new LibroManager();
        Libro libro = libroManager.crearLibro(isbn, titulo, paginas, precio, editorial.getCod_edi(), estado.getCod_estado());
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(libro.toString());
        
        System.out.print("ingresar precio neto: ");
        String precio_netoo = scanner.nextLine();
        int precio_neto = Integer.parseInt(precio_netoo);
        
        FacturaManager facturaManager = new FacturaManager();
        Factura factura = facturaManager.crearFactura(precio_neto, pago.getCod_pago(), distribuidor.getId_dis());
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(factura.toString());
        
        System.out.print("ingresar precio: ");
        String preci = scanner.nextLine();
        int precioC = Integer.parseInt(preci);
        
        System.out.print("ingresar CompraCol: ");
        String compracool = scanner.nextLine();
        int compacol = Integer.parseInt(compracool);
        
        CompraManager compraManager = new CompraManager();
        Compra compra = compraManager.crearCompra(precioC, factura.getFolio_fac(), libro.getPrecio(), compracool);
        System.out.println("Los datos han sido gurdados exitosamente");
        System.out.println(compra.toString());
        
               
    }
    

}
