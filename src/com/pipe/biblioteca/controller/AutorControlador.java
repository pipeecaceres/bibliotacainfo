/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.controller;

import com.pipe.biblioteca.modelos.Autor;
import com.pipe.biblioteca.modelos.AutorManager;
import java.util.List;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo comunicar la capa
 * vista con la capa modelo dentro del sw
 */
public class AutorControlador {

    private AutorManager aManager;

    public AutorControlador() {
        aManager = new AutorManager();
    }

    /**
     * comunica el metodo crearAutor del modelo con la vista
     *
     * @param nombre_au
     * @param ape_paterno
     * @param ape_materno
     * @return autores
     */
    public Autor crearAutor(String nombre_au, String ape_paterno, String ape_materno) {

        Autor autor = aManager.crearAutor(nombre_au, ape_paterno, ape_materno);

        return autor;
    }

    /**
     * metodo para llenar la tabla de autores dentro de la vista
     *
     * @return autores
     */
    public List<Autor> listarAutores() {

        List<Autor> autores = aManager.listarAutor();

        return autores;

    }

}
