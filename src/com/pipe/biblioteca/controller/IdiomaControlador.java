/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.controller;

import com.pipe.biblioteca.modelos.Idioma;
import com.pipe.biblioteca.modelos.IdiomaManager;
import java.util.List;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo comunicar la capa
 * vista con la capa modelo dentro del sw
 */
public class IdiomaControlador {
    
      private IdiomaManager iManager;
      
      public IdiomaControlador(){
          
          iManager = new IdiomaManager();
                  
      }
      /**
       * comunica el metodo creaIdioma de la capa modelo con la vista 
       * @param lengua
       * @return idioma
       */
      public Idioma crearIdioma(String lengua){
          
          Idioma idioma = iManager.crearIdioma(lengua);
          
          return idioma;
      }
      /**
       * llena la tabla idioma en la vista 
       * @return idiomas 
       */
      public List<Idioma> listarIdiomas(){
          
          List<Idioma> idiomas = iManager.listarIdioma();
          
          return idiomas;
          
      }
      
      
      
      
    
}
