/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase categoria a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class CategoriaManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public CategoriaManager() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * este metodo crea una fila dentro de la tabla categoria
     *
     * @param nombre
     * @return categoria
     *
     */
    public Categoria crearCategoria(String nombre) {

        Categoria categoria = new Categoria();
        categoria.setNombre(nombre);
        //guradar los datos en la bd
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(categoria);
        this.entityManager.getTransaction().commit();

        return categoria;
    }

}
