/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase editorial a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class EditorialManger {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public EditorialManger() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * este metodo crea una fila dentro de la tabla editorial
     *
     * @param nombre_edi
     * @return editorial
     *
     */
    public Editorial crearEditorial(String nombre_edi) {

        Editorial editorial = new Editorial();
        editorial.setNombre_edi(nombre_edi);
        //guarda los datos en la bd   
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(editorial);
        this.entityManager.getTransaction().commit();

        return editorial;
    }

}
