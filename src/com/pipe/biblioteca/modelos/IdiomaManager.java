/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase idioma a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class IdiomaManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public IdiomaManager() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * este metodo crea una fila dentro de la tabla idioma
     *
     * @param lengua
     * @return
     *
     */
    public Idioma crearIdioma(String lengua) {

        Idioma idioma = new Idioma();
        //agregar datos al idioma
        idioma.setLengua(lengua);
        //guardar los datos en la bd
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(idioma);
        this.entityManager.getTransaction().commit();

        return idioma;
    }

    /**
     * este metodo lista lo guardado en la tabla idioma en la vista
     *
     * @return idiomas
     */
    public List<Idioma> listarIdioma() {
        List<Idioma> idiomas = entityManager.createQuery("SELECT i FROM Idioma i").getResultList();

        return idiomas;

    }

    /**
     * este metodo elimina una lista dentro de la tabla idioma
     *
     * @param cod_idioma
     * @return true
     */
    public boolean eliminarIdioma(int cod_idioma) {
        Idioma idioma = this.entityManager.find(Idioma.class, cod_idioma);

        this.entityManager.getTransaction().begin();
        this.entityManager.remove(idioma);
        this.entityManager.getTransaction().commit();
        return true;

    }

    /**
     * este metodo edita los datos de una fila dentro de la tabla idioma
     *
     * @param cod_idioma
     * @param lengua
     * @return true
     */
    public boolean editarIdioma(int cod_idioma, String lengua) {
        Idioma idioma = this.entityManager.find(Idioma.class, cod_idioma);

        idioma.setLengua(lengua);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(idioma);
        this.entityManager.getTransaction().commit();
        return true;

    }

}
