/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * distribuidor en la tabla distribuidor de la db
 */
@Entity
@Table(name = "distribuidor")
public class Distribuidor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_dis;

    private String nombre_dis;
    private String calle;
    private int numero;
    private String comuna;
    private String pais;
    private int fono;

    @Temporal(TemporalType.TIMESTAMP)
    private Date año_contrato;

    private String rut;

    /**
     *
     */
    public Distribuidor() {
    }

    /**
     *
     * @param id_dis
     * @param nombre_dis
     * @param calle
     * @param numero
     * @param comuna
     * @param pais
     * @param fono
     * @param año_contrato
     * @param rut
     */
    public Distribuidor(int id_dis, String nombre_dis, String calle, int numero, String comuna, String pais, int fono, Date año_contrato, String rut) {
        this.id_dis = id_dis;
        this.nombre_dis = nombre_dis;
        this.calle = calle;
        this.numero = numero;
        this.comuna = comuna;
        this.pais = pais;
        this.fono = fono;
        this.año_contrato = año_contrato;
        this.rut = rut;
    }

    /**
     *
     * @return
     */
    public int getId_dis() {
        return id_dis;
    }

    /**
     *
     * @param id_dis
     */
    public void setId_dis(int id_dis) {
        this.id_dis = id_dis;
    }

    /**
     *
     * @return
     */
    public String getNombre_dis() {
        return nombre_dis;
    }

    /**
     *
     * @param nombre_dis
     */
    public void setNombre_dis(String nombre_dis) {
        this.nombre_dis = nombre_dis;
    }

    /**
     *
     * @return
     */
    public String getCalle() {
        return calle;
    }

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     *
     * @return
     */
    public int getNumero() {
        return numero;
    }

    /**
     *
     * @param numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     *
     * @return
     */
    public String getComuna() {
        return comuna;
    }

    /**
     *
     * @param comuna
     */
    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    /**
     *
     * @return
     */
    public String getPais() {
        return pais;
    }

    /**
     *
     * @param pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     *
     * @return
     */
    public int getFono() {
        return fono;
    }

    /**
     *
     * @param fono
     */
    public void setFono(int fono) {
        this.fono = fono;
    }

    /**
     *
     * @return
     */
    public Date getAño_contrato() {
        return año_contrato;
    }

    /**
     *
     * @param año_contrato
     */
    public void setAño_contrato(Date año_contrato) {
        this.año_contrato = año_contrato;
    }

    /**
     *
     * @return
     */
    public String getRut() {
        return rut;
    }

    /**
     *
     * @param rut
     */
    public void setRut(String rut) {
        this.rut = rut;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Distribuidor{" + "id_dis=" + id_dis + ", nombre_dis=" + nombre_dis + ", calle=" + calle + ", numero=" + numero + ", comuna=" + comuna + ", pais=" + pais + ", fono=" + fono + ", a\u00f1o_contrato=" + año_contrato + ", rut=" + rut + '}';
    }

}
