/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * idioma en la tabla idioma de la db
 */
@Entity
@Table(name = "idioma")
public class Idioma {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_idioma;

    private String lengua;

    /**
     *
     */
    public Idioma() {
    }

    /**
     *
     * @param cod_idioma
     * @param lengua
     */
    public Idioma(int cod_idioma, String lengua) {
        this.cod_idioma = cod_idioma;
        this.lengua = lengua;
    }

    /**
     *
     * @return
     */
    public int getCod_idioma() {
        return cod_idioma;
    }

    /**
     *
     * @param cod_idioma
     */
    public void setCod_idioma(int cod_idioma) {
        this.cod_idioma = cod_idioma;
    }

    /**
     *
     * @return
     */
    public String getLengua() {
        return lengua;
    }

    /**
     *
     * @param lengua
     */
    public void setLengua(String lengua) {
        this.lengua = lengua;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Idioma{" + "cod_idioma=" + cod_idioma + ", lengua=" + lengua + '}';
    }

}
