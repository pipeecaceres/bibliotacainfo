/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 * esta clase tiene como objetivo instanciar la clase estado en la tabla estado de la db
 */
@Entity
@Table(name = "estado")
public class Estado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_estado;
    
    private String descripcion;

    /**
     *
     */
    public Estado() {
    }

    /**
     *
     * @param cod_estado
     * @param detalle
     */
    public Estado(int cod_estado, String detalle) {
        this.cod_estado = cod_estado;
        this.descripcion = detalle;
    }

    /**
     *
     * @return
     */
    public int getCod_estado() {
        return cod_estado;
    }

    /**
     *
     * @param cod_estado
     */
    public void setCod_estado(int cod_estado) {
        this.cod_estado = cod_estado;
    }

    /**
     *
     * @return
     */
    public String getDetalle() {
        return descripcion;
    }

    /**
     *
     * @param detalle
     */
    public void setDetalle(String detalle) {
        this.descripcion = detalle;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Estado{" + "cod_estado=" + cod_estado + ", descripcion=" + descripcion + '}';
    }
    
    
    
    
}
