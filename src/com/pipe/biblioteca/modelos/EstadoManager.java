/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase estado a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class EstadoManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public EstadoManager() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }

    /**
     * este metodo crea una fila dentro de la tabla estado
     *
     * @param descripcion
     * @return
     *
     */
    public Estado crearEstado(String descripcion) {

        Estado estado = new Estado();
        estado.setDetalle(descripcion);
        //guarda los datos en la bd
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(estado);
        this.entityManager.getTransaction().commit();

        return estado;
    }
}
