/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * libro en la tabla libro de la db
 */
@Entity
@Table(name = "libro")
public class Libro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int serie;

    private String isbn;
    private String titulo;
    private int paginas;
    private int precio;

    @Temporal(TemporalType.DATE)
    private Date año;

    private int editorial_cod_edit;
    private int estado_cod_estado;

    @ManyToMany
    @JoinTable(
            name = "autor_libro",
            joinColumns = @JoinColumn(name = "libro_serie", referencedColumnName = "serie"),
            inverseJoinColumns = @JoinColumn(name = "autor_cod_autor", referencedColumnName = "cod_autor"))
    private List<Autor> autor;

    //Tabla intermedia entre categoria y libro
    @ManyToMany
    @JoinTable(
            name = "categoria_libro",
            joinColumns = @JoinColumn(name = "libro_serie", referencedColumnName = "serie"),
            inverseJoinColumns = @JoinColumn(name = "categoria_cod_cate", referencedColumnName = "cod_cate"))
    private List<Categoria> categoria;

    @ManyToMany
    @JoinTable(
            name = "idioma_libro",
            joinColumns = @JoinColumn(name = "libro_serie", referencedColumnName = "serie"),
            inverseJoinColumns = @JoinColumn(name = "idioma_cod_idioma", referencedColumnName = "cod_idioma"))
    private List<Idioma> idioma;

    /**
     *
     */
    public Libro() {
    }

    /**
     *
     * @param serie
     * @param isbn
     * @param titulo
     * @param paginas
     * @param precio
     * @param año
     * @param editorial_cod_edit
     * @param estado_cod_estado
     */
    public Libro(int serie, String isbn, String titulo, int paginas, int precio, Date año, int editorial_cod_edit, int estado_cod_estado) {
        this.serie = serie;
        this.isbn = isbn;
        this.titulo = titulo;
        this.paginas = paginas;
        this.precio = precio;
        this.año = año;
        this.editorial_cod_edit = editorial_cod_edit;
        this.estado_cod_estado = estado_cod_estado;
    }

    /**
     *
     * @return
     */
    public int getSerie() {
        return serie;
    }

    /**
     *
     * @param serie
     */
    public void setSerie(int serie) {
        this.serie = serie;
    }

    /**
     *
     * @return
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     *
     * @param isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     *
     * @return
     */
    public int getPaginas() {
        return paginas;
    }

    /**
     *
     * @param paginas
     */
    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    /**
     *
     * @return
     */
    public int getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /**
     *
     * @return
     */
    public Date getAño() {
        return año;
    }

    /**
     *
     * @param año
     */
    public void setAño(Date año) {
        this.año = año;
    }

    /**
     *
     * @return
     */
    public int getEditorial_cod_edit() {
        return editorial_cod_edit;
    }

    /**
     *
     * @param editorial_cod_edit
     */
    public void setEditorial_cod_edit(int editorial_cod_edit) {
        this.editorial_cod_edit = editorial_cod_edit;
    }

    /**
     *
     * @return
     */
    public int getEstado_cod_estado() {
        return estado_cod_estado;
    }

    /**
     *
     * @param estado_cod_estado
     */
    public void setEstado_cod_estado(int estado_cod_estado) {
        this.estado_cod_estado = estado_cod_estado;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Libro{" + "serie=" + serie + ", isbn=" + isbn + ", titulo=" + titulo + ", paginas=" + paginas + ", precio=" + precio + ", a\u00f1o=" + año + ", editorial_cod_edit=" + editorial_cod_edit + ", estado_cod_estado=" + estado_cod_estado + '}';
    }

}
