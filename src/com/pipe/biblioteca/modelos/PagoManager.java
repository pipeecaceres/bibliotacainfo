/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase pago a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class PagoManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public PagoManager() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * este metodo crea una fila dentro de la tabla pago
     *
     * @param descripcion_pag
     * @return pago
     *
     */
    public Pago crearPago(String descripcion_pag) {

        Pago pago = new Pago();
        pago.setDescripcion_pag(descripcion_pag);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(pago);
        this.entityManager.getTransaction().commit();

        return pago;
    }

}
