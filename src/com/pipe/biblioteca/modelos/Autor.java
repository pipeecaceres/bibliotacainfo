/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * autor en la tabla autor de la db
 */
@Entity
@Table(name = "autor")
public class Autor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_autor;

    private String nombre_au;
    private String ape_paterno;
    private String ape_materno;

    /**
     *
     */
    public Autor() {
    }

    /**
     *
     * @param cod_autor
     * @param nombre_au
     * @param ape_paterno
     * @param ape_materno
     */
    public Autor(int cod_autor, String nombre_au, String ape_paterno, String ape_materno) {
        this.cod_autor = cod_autor;
        this.nombre_au = nombre_au;
        this.ape_paterno = ape_paterno;
        this.ape_materno = ape_materno;
    }

    /**
     *
     * @return
     */
    public int getCod_autor() {
        return cod_autor;
    }

    /**
     *
     * @param cod_autor
     */
    public void setCod_autor(int cod_autor) {
        this.cod_autor = cod_autor;
    }

    /**
     *
     * @return
     */
    public String getNombre_au() {
        return nombre_au;
    }

    /**
     *
     * @param nombre_au
     */
    public void setNombre_au(String nombre_au) {
        this.nombre_au = nombre_au;
    }

    /**
     *
     * @return
     */
    public String getApe_paterno() {
        return ape_paterno;
    }

    /**
     *
     * @param ape_paterno
     */
    public void setApe_paterno(String ape_paterno) {
        this.ape_paterno = ape_paterno;
    }

    /**
     *
     * @return
     */
    public String getApe_materno() {
        return ape_materno;
    }

    /**
     *
     * @param ape_materno
     */
    public void setApe_materno(String ape_materno) {
        this.ape_materno = ape_materno;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Autor{" + "cod_autor=" + cod_autor + ", nombre_au=" + nombre_au + ", ape_paterno=" + ape_paterno + ", ape_materno=" + ape_materno + '}';
    }

}
