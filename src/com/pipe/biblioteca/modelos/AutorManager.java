/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clases sirve para enlazar la clase y sus funciones con la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 */
public class AutorManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public AutorManager() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * inserta una fila dentro de la tabla autor
     *
     * @param nombre_au
     * @param ape_paterno
     * @param ape_materno
     * @return autor
     */
    public Autor crearAutor(String nombre_au, String ape_paterno, String ape_materno) {
        Autor autor = new Autor();

        autor.setNombre_au(nombre_au);
        autor.setApe_paterno(ape_paterno);
        autor.setApe_materno(ape_materno);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(autor);
        this.entityManager.getTransaction().commit();

        return autor;
    }

    /**
     * metodo para llenar la tabla de autores dentro de la vista
     *
     * @return autores
     */
    public List<Autor> listarAutor() {

        List<Autor> autores = entityManager.createQuery("SELECT a FROM Autor a").getResultList();

        return autores;

    }

    /**
     * metodo para borrar un autor seleccionado dentro de la
     *
     * @param cod_autor base de datos
     * @return true
     */
    public boolean eliminarAutor(int cod_autor) {

        Autor autor = this.entityManager.find(Autor.class, cod_autor);

        this.entityManager.getTransaction().begin();
        this.entityManager.remove(autor);
        this.entityManager.getTransaction().commit();
        return true;
    }

    /**
     * medoto para editar los datos de un autor dentro de la db
     *
     * @param cod_autor
     * @param nombre_au
     * @param ape_paterno
     * @param ape_materno
     * @return true
     */
    public boolean editarAutor(int cod_autor, String nombre_au, String ape_paterno, String ape_materno) {

        Autor autor = this.entityManager.find(Autor.class, cod_autor);

        autor.setNombre_au(nombre_au);
        autor.setApe_paterno(ape_paterno);
        autor.setApe_materno(ape_materno);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(autor);
        this.entityManager.getTransaction().commit();
        return true;
    }

}
