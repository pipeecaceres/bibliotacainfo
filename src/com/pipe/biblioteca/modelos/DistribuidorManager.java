/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 * esta clase sirve para enlazar la clase distribuidor a la db
 */
public class DistribuidorManager {
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    public DistribuidorManager(){
        
        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    
    }
    
     /**
     * este metodo crea una fila dentro de la tabla distribuidor
     * @param nombre_dis
     * @param calle
     * @param numero
     * @param comuna
     * @param pais
     * @param fono
     * @param rut
     * @return distribuidor
     */
    public Distribuidor crearDistribuidor(String nombre_dis, String calle, int numero, String comuna, String pais, int fono, String rut){
        Distribuidor distribuidor = new Distribuidor();
        
        distribuidor.setNombre_dis(nombre_dis);
        distribuidor.setCalle(calle);
        distribuidor.setNumero(numero);
        distribuidor.setComuna(comuna);
        distribuidor.setPais(pais);
        distribuidor.setFono(fono);
        distribuidor.setRut(rut);
        
         this.entityManager.getTransaction().begin();
        this.entityManager.persist(distribuidor);
        this.entityManager.getTransaction().commit();
        
        return distribuidor;
    }
            
    
}
