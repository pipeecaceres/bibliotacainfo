/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * factura en la tabla factura de la db
 */
@Entity
@Table(name = "factura")
public class Factura {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int folio_fac;

    private int precio_neto;

    @Temporal(TemporalType.DATE)
    private Date fecha_compra;

    @Temporal(TemporalType.TIME)
    private Date hora;

    private int meotodo_pago_cod_pago;
    private int distribuidor_id;

    /**
     *
     */
    public Factura() {
    }

    /**
     *
     * @param folio_fac
     * @param precio_neto
     * @param fecha_compra
     * @param hora
     * @param metodo_pago_cod_pag
     * @param distribuidor_id
     */
    public Factura(int folio_fac, int precio_neto, Date fecha_compra, Date hora, int metodo_pago_cod_pag, int distribuidor_id) {
        this.folio_fac = folio_fac;
        this.precio_neto = precio_neto;
        this.fecha_compra = fecha_compra;
        this.hora = hora;
        this.meotodo_pago_cod_pago = meotodo_pago_cod_pago;
        this.distribuidor_id = distribuidor_id;
    }

    /**
     *
     * @return
     */
    public int getFolio_fac() {
        return folio_fac;
    }

    /**
     *
     * @param folio_fac
     */
    public void setFolio_fac(int folio_fac) {
        this.folio_fac = folio_fac;
    }

    /**
     *
     * @return
     */
    public int getPrecio_neto() {
        return precio_neto;
    }

    /**
     *
     * @param precio_neto
     */
    public void setPrecio_neto(int precio_neto) {
        this.precio_neto = precio_neto;
    }

    /**
     *
     * @return
     */
    public Date getFecha_compra() {
        return fecha_compra;
    }

    /**
     *
     * @param fecha_compra
     */
    public void setFecha_compra(Date fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    /**
     *
     * @return
     */
    public Date getHora() {
        return hora;
    }

    /**
     *
     * @param hora
     */
    public void setHora(Date hora) {
        this.hora = hora;
    }

    /**
     *
     * @return
     */
    public int getMeotodo_pago_cod_pago() {
        return meotodo_pago_cod_pago;
    }

    /**
     *
     * @param meotodo_pago_cod_pago
     */
    public void setMeotodo_pago_cod_pago(int meotodo_pago_cod_pago) {
        this.meotodo_pago_cod_pago = meotodo_pago_cod_pago;
    }

    /**
     *
     * @return
     */
    public int getDistribuidor_id() {
        return distribuidor_id;
    }

    /**
     *
     * @param distribuidor_id
     */
    public void setDistribuidor_id(int distribuidor_id) {
        this.distribuidor_id = distribuidor_id;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Factura{" + "folio_fac=" + folio_fac + ", precio_neto=" + precio_neto + ", fecha_compra=" + fecha_compra + ", hora=" + hora + ", meotodo_pago_cod_pago=" + meotodo_pago_cod_pago + ", distribuidor_id=" + distribuidor_id + '}';
    }

}
