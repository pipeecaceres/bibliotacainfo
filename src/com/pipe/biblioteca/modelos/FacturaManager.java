/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase factura a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class FacturaManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public FacturaManager() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();

    }

    /**
     * este metodo crea una fila en la tabla factura
     *
     * @param precio_neto
     * @param meotodo_pago_cod_pago
     * @param distribuidor_id
     * @return factura
     */
    public Factura crearFactura(int precio_neto, int meotodo_pago_cod_pago, int distribuidor_id) {
        Factura factura = new Factura();

        factura.setPrecio_neto(precio_neto);
        factura.setMeotodo_pago_cod_pago(meotodo_pago_cod_pago);
        factura.setDistribuidor_id(distribuidor_id);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(factura);
        this.entityManager.getTransaction().commit();

        return factura;
    }
}
