/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * categoria en la tabla categoria de la db
 */
@Entity
@Table(name = "categoria")

public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_cate;

    private String nombre;

    /**
     *
     */
    public Categoria() {
    }

    /**
     *
     * @param cod_cate
     * @param nombre
     */
    public Categoria(int cod_cate, String nombre) {
        this.cod_cate = cod_cate;
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public int getCod_cate() {
        return cod_cate;
    }

    /**
     *
     * @param cod_cate
     */
    public void setCod_cate(int cod_cate) {
        this.cod_cate = cod_cate;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Categoria{" + "cod_cate=" + cod_cate + ", nombre=" + nombre + '}';
    }

}
