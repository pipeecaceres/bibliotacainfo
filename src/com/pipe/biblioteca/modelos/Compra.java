/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * compra en la tabla compra de la db
 */
@Entity
@Table(name = "compra")
public class Compra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_comp;

    private int libro_serie;
    private int factura_folio_fac;
    private int precio;
    private String compracol;

    /**
     *
     */
    public Compra() {
    }

    /**
     *
     * @param id_comp
     * @param libro_serie
     * @param factura_folio_fac
     * @param precio
     * @param compracol
     */
    public Compra(int id_comp, int libro_serie, int factura_folio_fac, int precio, String compracol) {
        this.id_comp = id_comp;
        this.libro_serie = libro_serie;
        this.factura_folio_fac = factura_folio_fac;
        this.precio = precio;
        this.compracol = compracol;
    }

    /**
     *
     * @return
     */
    public int getId_comp() {
        return id_comp;
    }

    /**
     *
     * @param id_comp
     */
    public void setId_comp(int id_comp) {
        this.id_comp = id_comp;
    }

    /**
     *
     * @return
     */
    public int getLibro_serie() {
        return libro_serie;
    }

    /**
     *
     * @param libro_serie
     */
    public void setLibro_serie(int libro_serie) {
        this.libro_serie = libro_serie;
    }

    /**
     *
     * @return
     */
    public int getFactura_folio_fac() {
        return factura_folio_fac;
    }

    /**
     *
     * @param factura_folio_fac
     */
    public void setFactura_folio_fac(int factura_folio_fac) {
        this.factura_folio_fac = factura_folio_fac;
    }

    /**
     *
     * @return
     */
    public int getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /**
     *
     * @return
     */
    public String getCompracol() {
        return compracol;
    }

    /**
     *
     * @param compracol
     */
    public void setCompracol(String compracol) {
        this.compracol = compracol;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Compra{" + "id_comp=" + id_comp + ", libro_serie=" + libro_serie + ", factura_folio_fac=" + factura_folio_fac + ", precio=" + precio + ", compracol=" + compracol + '}';
    }

}
