/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase lbro a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class LibroManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public LibroManager() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }

    /**
     * este metodo crea una fila dentro de la tabla libro en la base de datos
     *
     * @param isbn
     * @param titulo
     * @param paginas
     * @param precio
     * @param editorial_cod_edit
     * @param estado_cod_estado
     *
     * @return libro
     */
    public Libro crearLibro(String isbn, String titulo, int paginas, int precio, int editorial_cod_edit, int estado_cod_estado) {

        Libro libro = new Libro();

        libro.setIsbn(isbn);
        libro.setTitulo(titulo);
        libro.setPaginas(paginas);
        libro.setPrecio(precio);
        libro.setEditorial_cod_edit(editorial_cod_edit);
        libro.setEstado_cod_estado(estado_cod_estado);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(libro);
        this.entityManager.getTransaction().commit();

        return libro;
    }
}
