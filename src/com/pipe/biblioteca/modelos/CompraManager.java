/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * esta clase sirve para enlazar la clase compra a la db
 *
 * @author felipe
 * @version 1.0.0 12/07/2017
 *
 */
public class CompraManager {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     *
     */
    public CompraManager() {

        this.entityManagerFactory = Persistence.createEntityManagerFactory("bibliotecaPU");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }

    /**
     * este metodo crea una fila en la tabla compra
     *
     * @param libro_serie
     * @param factura_folio_fac
     * @param precioC
     * @param compracol
     * @return compra
     */
    public Compra crearCompra(int libro_serie, int factura_folio_fac, int precioC, String compracol) {

        Compra compra = new Compra();

        compra.setLibro_serie(libro_serie);
        compra.setFactura_folio_fac(factura_folio_fac);
        compra.setPrecio(precioC);
        compra.setCompracol(compracol);

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(compra);
        this.entityManager.getTransaction().commit();

        return compra;

    }
}
