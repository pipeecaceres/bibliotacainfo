/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * editorial en la tabla editorial de la db
 */
@Entity
@Table(name = "editorial")
public class Editorial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_edi;

    private String nombre_edi;

    /**
     *
     */
    public Editorial() {
    }

    /**
     *
     * @param cod_edi
     * @param nombre_edi
     */
    public Editorial(int cod_edi, String nombre_edi) {
        this.cod_edi = cod_edi;
        this.nombre_edi = nombre_edi;
    }

    /**
     *
     * @return
     */
    public int getCod_edi() {
        return cod_edi;
    }

    /**
     *
     * @param cod_edi
     */
    public void setCod_edi(int cod_edi) {
        this.cod_edi = cod_edi;
    }

    /**
     *
     * @return
     */
    public String getNombre_edi() {
        return nombre_edi;
    }

    /**
     *
     * @param nombre_edi
     */
    public void setNombre_edi(String nombre_edi) {
        this.nombre_edi = nombre_edi;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Editorial{" + "cod_edi=" + cod_edi + ", nombre_edi=" + nombre_edi + '}';
    }

}
