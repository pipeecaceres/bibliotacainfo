/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pipe.biblioteca.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author felipe
 * @version 1.0.0 12/07/2017 esta clase tiene como objetivo instanciar la clase
 * pago en la tabla pago de la db
 */
@Entity
@Table(name = "meotodo_pago")
public class Pago {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_pago;

    private String descripcion_pag;

    /**
     *
     */
    public Pago() {
    }

    /**
     *
     * @param cod_pago
     * @param descripcion_pag
     */
    public Pago(int cod_pago, String descripcion_pag) {
        this.cod_pago = cod_pago;
        this.descripcion_pag = descripcion_pag;
    }

    /**
     *
     * @return
     */
    public int getCod_pago() {
        return cod_pago;
    }

    /**
     *
     * @param cod_pago
     */
    public void setCod_pago(int cod_pago) {
        this.cod_pago = cod_pago;
    }

    /**
     *
     * @return
     */
    public String getDescripcion_pag() {
        return descripcion_pag;
    }

    /**
     *
     * @param descripcion_pag
     */
    public void setDescripcion_pag(String descripcion_pag) {
        this.descripcion_pag = descripcion_pag;
    }

}
